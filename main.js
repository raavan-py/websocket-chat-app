const connection = new WebSocket('ws://localhost:8000');

connection.onopen = () => {
  console.log('connected');
};

connection.onclose = () => {
  console.error('disconnected');
};

connection.onerror = (error) => {
  console.error('failed to connect', error);
};

connection.onmessage = (event) => {
  console.log('received', event.data);
  let li = document.createElement('p');
  li.innerText = event.data;
  document.querySelector('#chat').append(li);
};

document.querySelector("button").addEventListener('click', (event) => {
  event.preventDefault();
  let message = document.querySelector('#message').value;
  if(!message) return;
  connection.send(message);
  document.querySelector('#message').value = '';
});
 