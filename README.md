# A Simple WebSocket Chat App in Node.js

- First Clone Repository 

```bash
git clone https://raavan-py@bitbucket.org/raavan-py/websocket-chat-app.git
```
- Go to WebSocket Chat App Directory

```bash
cd websocket-chat-app
```
- Create a file name *package.json*

```json
{
  "dependencies": {
    "ws": "^7.0.1"
  }
}
```
- install ws: Type command 

```bash
npm install
```
- Running the backend server.js type command in terminal 

```bash
node server.js
```

- Next, open up two browser tabs with index.html *( Default Port 8000 )*
 